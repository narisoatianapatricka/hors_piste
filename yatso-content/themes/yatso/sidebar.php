<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

?>
		<?php if ( is_active_sidebar('sidebar-primary') ) : ?>	
<div class="col-md-4 col-xs-12">
	<div class="sidebar">
  <?php dynamic_sidebar('sidebar-primary' );  ?>
	</div>
</div>	
<?php endif;?>