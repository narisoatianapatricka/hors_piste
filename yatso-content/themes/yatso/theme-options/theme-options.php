<?php
/**
 * Theme options
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */
global $yatso_options;
require_once get_template_directory() . '/theme-options/options.php';
$yatso_options = get_option( 'yatso_theme_options' );

add_action( 'admin_init', 'yatso_options_init' );
function yatso_options_init(){
 register_setting( 'yatso_options', 'yatso_theme_options','yatso_options_validate');
} 

function yatso_options_validate($input)
{
   $allfields_settings = yatso_get_all_settings();

   foreach ( $input as $i ){
     if ( isset($allfields_settings[$i]) ){
        switch ( $allfields_settings[$i]['type'] ){
          case 'text':
            $input[$i] = sanitize_text_field( $input[$i] );
            break;
          case 'select':
            break;
          case 'date':
            break;
          case 'url':
            $input[$i] = esc_url_raw( $input[$i] );
            break;
          case 'textarea':
            $input[$i] = sanitize_text_field( $input[$i] );
            break;
          case 'image':
            $input[$i] = yatso_image_validation(esc_url_raw( $input[$i]));
            break;
          default:
        }
     }
   }

	  return $input;
}
function yatso_image_validation($yatso_imge_url){
	$yatso_filetype = wp_check_filetype($yatso_imge_url);
	$yatso_supported_image = array('gif','jpg','jpeg','png','ico');
	if (in_array($yatso_filetype['ext'], $yatso_supported_image)) {
		return $yatso_imge_url;
	} else {
	return '';
	}
}
function yatso_get_all_settings(){
  global $yatso_options_settings;
  $allfields = array();
  foreach ( $yatso_options_settings as $tab) {
      $allfields = array_merge( $allfields, $tab );
  }
  return $allfields;
}

add_action( 'admin_enqueue_scripts', 'yatso_framework_load_scripts' );
function yatso_framework_load_scripts(){
	wp_enqueue_media();
	wp_enqueue_style( 'yatso_framework', get_template_directory_uri(). '/theme-options/css/theme-options.css' ,false, '1.0.0');
	// Enqueue custom option panel JS
	wp_enqueue_script( 'options-custom', get_template_directory_uri(). '/theme-options/js/theme-options.js', array( 'jquery' ) );
	wp_enqueue_script( 'media-uploader', get_template_directory_uri(). '/theme-options/js/media-uploader.js', array( 'jquery') );		
}

add_action( 'admin_menu', 'yatso_options_add_page' );
function yatso_options_add_page() {
	add_theme_page( 'yatso Options', 'Theme Options', 'edit_theme_options', 'yatso_framework', 'yatso_framework_page');
}

function yatso_framework_page(){
  include 'admin-page.php';
}

//compatibilité WPML, rendre les options translatables
if ( function_exists('icl_register_string')){
  add_action('wp_ajax_icl_tl_rescan', 'yatso_options_wpml_translate');
  function yatso_options_wpml_translate(){
    $theme_options = get_option( 'yatso_theme_options' );
    foreach ( $theme_options as $key => $option ){
      if ( intval($option)>0 ) continue;
      if ( !is_string($option) ) continue;
      icl_register_string( 'yatso_options', $key, apply_filters('widget_text', $option));
    }
  }
}