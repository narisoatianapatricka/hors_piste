<?php
/**
 * yatso functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, yatso_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'yatso_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

require_once( get_template_directory() . '/inc/constante.inc.php' );
require_once( get_template_directory() . '/inc/default.config.php' );
require_once( get_template_directory() . '/inc/utils/functions.php' );
require_once( get_template_directory() . '/login.php' );


//require new theme 

require_once('theme_setup_data.php');

	//Files for custom - defaults menus
	require_once( get_template_directory() . '/inc/functions/menu/busiprof_nav_walker.php' );
	require_once( get_template_directory() . '/inc/functions/menu/default_menu_walker.php' );
	require_once( get_template_directory() . '/inc/functions/woo/woocommerce.php' );
	require_once( get_template_directory() . '/inc/functions/font/font.php' );
	require_once( get_template_directory() . '/inc/functions/breadcrumbs/breadcrumbs.php');


	// Theme functions file including
	require_once( get_template_directory() . '/inc/functions/scripts/script.php');
	require_once( get_template_directory() . '/inc/functions/widgets/custom-widgets.php' ); // for footer widget
	require_once( get_template_directory() . '/inc/functions/commentbox/comment-function.php' ); // for custom contact widget

	// customizer files include
	require_once( get_template_directory() . '/inc/functions/customizer/custo_general_settings.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/custo_sections_settings.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/custo_template_settings.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/custo_post_slugs_settings.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/custo_layout_manager_settings.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/cust_pro.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/custo_emailcourse.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/customizer.php' );
	require_once( get_template_directory() . '/inc/functions/customizer/customizer-archive.php');

	//Fin required new theme 


//classes de service
require_once_files_in( get_template_directory() . '/inc/classes/posttype' );
require_once_files_in( get_template_directory() . '/inc/classes/taxonomy' );
require_once_files_in( get_template_directory() . '/inc/classes/user' );

if (is_admin()){
  require_once( get_template_directory() . '/admin-functions.php' );

  /*** Theme Option ***/
  if ( is_dir( get_template_directory() . '/theme-options' ) ){
    require get_template_directory() . '/theme-options/theme-options.php';
  }
}

//lib
require_once( get_template_directory() . '/lib/cssmin.php' );
require_once( get_template_directory()  . '/lib/jsmin.php' );

global $yatso_options;
$yatso_options = get_option( 'yatso_theme_options' );

/**
 * Tell WordPress to run yatso_setup() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'yatso_setup' );

if ( ! function_exists( 'yatso_setup' ) ):
function yatso_setup() {

    require_once_files_in( get_template_directory() . '/inc/extends/custom-sidebar' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-fields/acf' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-metaboxes' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-rules' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-role' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-mce-tools' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-shortcodes' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-sidebar' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-types' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-taxonomies' );
    require_once_files_in( get_template_directory() . '/inc/extends/custom-widgets' );

	/* Make yatso available for translation.
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on yatso, use a find and replace
	 * to change 'yatso' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'yatso', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	//add_editor_style();

	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'yatso' ) );

	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
	add_theme_support( 'post-thumbnails' );

	add_image_size( IMAGE_SIZE_ACTUS_VIGNETTE, 100, 100, true );
	add_image_size( IMAGE_SIZE_ACTUS_MEDIUM, 500, 300 );
	add_image_size( IMAGE_SIZE_BANIERE_CIRCUIT, 263, 260 );
	add_image_size( IMAGE_SIZE_TIMELINE_CIRCUIT, 263, 260 );

}
endif; // yatso_setup

// //theme ckeck plugin required 	
// 	add_theme_support( 'automatic-feed-links' );
// 	add_theme_support('woocommerce');
	
// 	//content width
// 	if ( ! isset( $content_width ) ) $content_width = 750;	


// 	if ( ! function_exists( 'busiporf_setup' ) ) :
// 	function busiporf_setup() {
	
// 	/*
// 	 * Make theme available for translation.
// 	 * Translations can be filed in the /languages/ directory.
// 	 */
// 	load_theme_textdomain( 'busiprof', get_template_directory() . '/lang' );
	
// 	// Add default posts and comments RSS feed links to head.
// 	add_theme_support( 'automatic-feed-links' );
	
	
// 	// Add theme support for selective refresh for widgets.
// 		add_theme_support( 'customize-selective-refresh-widgets' );
// 	/*
// 	 * Let WordPress manage the document title.
// 	 */
// 	add_theme_support( 'title-tag' );
	
// 	// supports featured image
// 	add_theme_support( 'post-thumbnails' );
	
// 	//Added Woocommerce Galllery Support
// 	add_theme_support( 'wc-product-gallery-zoom' );
// 	add_theme_support( 'wc-product-gallery-lightbox' );
// 	add_theme_support( 'wc-product-gallery-slider' );
	
		
// 	add_theme_support( 'custom-header');
	
// 	// This theme uses wp_nav_menu() in two locations.
// 	register_nav_menus( array(
// 		'primary' => __( 'Primary Menu', 'busiprof' )
// 	) );
	
	
// } // busiporf_setup
// endif;
	
// 	add_action( 'after_setup_theme', 'busiporf_setup' );
	
	
// 	function busiprof_inline_style() {
// 	$custom_css              = '';
	
	
// 	$busiprof_service_content = get_theme_mod(
// 		'busiprof_service_content', json_encode(
// 			array(
// 				array(
// 					'color'      => '#e91e63',
// 				),
// 				array(
// 					'color'      => '#00bcd4',
// 				),
// 				array(
// 					'color'      => '#4caf50',
// 				),
// 			)
// 		)
// 	);
	
// 	if ( ! empty( $busiprof_service_content ) ) {
// 		$busiprof_service_content = json_decode( $busiprof_service_content );
		
		
// 		foreach ( $busiprof_service_content as $key => $features_item ) {
// 			$box_nb = $key + 1;
// 			if ( ! empty( $features_item->color ) ) {
				
// 				$color = ! empty( $features_item->color ) ? apply_filters( 'busiprof_translate_single_string', $features_item->color, 'Features section' ) : '';
				
// 				$custom_css .= '.service-box:nth-child(' . esc_attr( $box_nb ) . ') .service-icon {
//                             color: ' . esc_attr( $color ) . ';
// 				}';
				
				
// 			}
// 		}
// 	}
// 	wp_add_inline_style( 'style', $custom_css );
// }

// add_action( 'wp_enqueue_scripts', 'busiprof_inline_style' );	



// add_action( 'after_switch_theme', 'import_busiprof_child_theme_data_in_busiprof_theme' );
// /**
// * Import theme mods when switching from Busiprof child theme to Busiprof
// */
// function import_busiprof_child_theme_data_in_busiprof_theme() {

//     // Get the name of the previously active theme.
//     $previous_theme = strtolower( get_option( 'theme_switched' ) );

//     if ( ! in_array(
//         $previous_theme, array(
//             'vdequator',
// 			'vdperanto',
// 			'arzine',
// 			'lazyprof',
//         )
//     ) ) {
//         return;
//     }

//     // Get the theme mods from the previous theme.
//     $previous_theme_content = get_option( 'theme_mods_' . $previous_theme );

//     if ( ! empty( $previous_theme_content ) ) {
//         foreach ( $previous_theme_content as $previous_theme_mod_k => $previous_theme_mod_v ) {
//             set_theme_mod( $previous_theme_mod_k, $previous_theme_mod_v );
//         }
//     }
// } 


/**
 * Enqueue scripts and styles.
 */
add_action('wp_enqueue_scripts', 'yatso_scripts');
function yatso_scripts() {

	//css 
	wp_enqueue_style('yatso-bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('yatso-custom', get_template_directory_uri() . '/css/custom.css');
	wp_enqueue_style('yatso-drag-drop', get_template_directory_uri() . '/css/drag-drop.css');
	wp_enqueue_style('yatso-flexslider', get_template_directory_uri() . '/css/flexslider.css');
	wp_enqueue_style('yatso-yatso', get_template_directory_uri() . '/css/yatso.css');
	wp_enqueue_style('yatso-style', get_template_directory_uri() . '/style.css');

    // js footer
    wp_enqueue_script('yatso-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), false, true);
    wp_enqueue_script('yatso-custom-script', get_template_directory_uri() . '/js/custom.js', array('jquery'), false, true);
    wp_enqueue_script('yatso-html5-script', get_template_directory_uri() . '/js/html5.js', array('jquery'), false, true);
}

/* Désactive Gutenberg / utilisation de l'éditeur classique de WP */

add_filter('use_block_editor_for_post', '__return_false');