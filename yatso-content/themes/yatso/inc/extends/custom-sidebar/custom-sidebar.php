<?php
/**
 * Initialisation des custom sidebars
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */
function yatso_widgets_init() {

    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'yatso' ),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    //register other sidebar
    //http://generatewp.com/sidebar/
}
add_action( 'widgets_init', 'yatso_widgets_init' );