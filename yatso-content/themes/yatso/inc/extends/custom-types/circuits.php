<?php
/**
 * register post type circuits
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

add_action('init', 'yatso_init_actus', 1);
function yatso_init_actus(){
  //post type
  $labels = get_custom_post_type_labels( 'circuits', 'circuits', 1 );
  $data = array(
    // 'capabilities'         => wp_get_custom_posts_capabilities('post'),
		'supports'             => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'         => false,
		'exclude_from_search'  => false,
		'public'               => true,
		'show_ui'              => true,
		'show_in_nav_menus'    => true,
		'menu_icon'            => 'dashicons-welcome-widgets-menus',
		'menu_position'        => 6,
		'labels'               => $labels,
		'query_var'            => true,
	);
	register_post_type( POST_TYPE_CIRCUITS, $data);
}