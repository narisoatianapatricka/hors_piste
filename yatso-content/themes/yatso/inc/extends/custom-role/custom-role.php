<?php
/**
 * Déclaration des profils users
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

add_action('init', 'yatso_init_role');
function yatso_init_role(){
  add_role( USER_PROFILE_ADMIN, 'Administrateur');
  add_role( USER_PROFILE_WEBMASTER, 'Webmaster');
  add_role( USER_PROFILE_MEMBRE, 'Membre');
}