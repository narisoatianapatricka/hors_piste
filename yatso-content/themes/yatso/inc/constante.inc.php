<?php
/*
 * Definir ici les constantes applicatives
 *
 * Essayez de bien organiser la declaration des constantes :
 * en regroupant les mêmes types de constantes
 * en prefixant les mêmes types de constantes
 * et en mettant des commentaires
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

//post types
define( 'POST_TYPE_ARTICLE', 'article' );
define( 'POST_TYPE_ACTUALITE', 'actualite' );
define( 'POST_TYPE_PAGE', 'page' );
define( 'POST_TYPE_CIRCUITS', 'circuits' );

//taxonomies
define( 'TAXONOMOMY_CATEGORY', 'category' );
define( 'TAXONOMOMY_TAG', 'post_tag' );
define( 'TAXONOMY_TYPE_ACTUS', 'type_actualite' );

//champs personnalisés (meta value) : syntaxe FIELD_{type de post}_{nom du champ}
define ('FIELD_POST_POST_NOTE', 'post_note');
define ('FIELD_ACTUALITE_LIEU', 'actualite_lieu');
define ('FIELD_TYPE_ACTUS_ICONE', 'type_actus_icone');
define ('FIELD_USER_CIVILITE', 'user_civilite');
define ('FIELD_USER_DATE_NAISSANCE', 'user_date');
define ('FIELD_USER_ADRESSE', 'user_adresse');
define ('FIELD_USER_VILLE', 'user_ville');
define ('FIELD_USER_CP', 'user_cp');

define ('FIELD_BANNIERE_IMAGE', 'banniere_image');
define ('FIELD_TIMELINE_TEXTE', 'timeline_journee_texte');
define ('FIELD_TIMELINE_IMAGE', 'timeline_journee_image');
define ('FIELD_VIDEOS', 'videos');
define ('FIELD_CARTOGRAPHIE_MADA', 'cartographie_madagascar');
//user role
define ('USER_PROFILE_MEMBRE', 'subscriber');
define ('USER_PROFILE_WEBMASTER', 'webmaster');
define ('USER_PROFILE_ADMIN', 'administrator');

//image size
define ('IMAGE_SIZE_ACTUS_VIGNETTE', 'actualite_vignette');
define ('IMAGE_SIZE_ACTUS_MEDIUM', 'actualite_medium');
define ('IMAGE_SIZE_BANIERE_CIRCUIT', 'baniere_circuits_desktop');
define ('IMAGE_SIZE_TIMELINE_CIRCUIT', 'timelinecircuits_desktop');

//....