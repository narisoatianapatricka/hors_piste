<?php
/**
 * Template Name: Contact
 *
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

get_header(); 
	global $post;
	$current_options = wp_parse_args(  get_option( 'busiprof_theme_options', array() ), $busiprof_theme_options );
	// $circuits        = CCircuits::getById($post->ID);
	$circuits        = CCircuits::getById(41);
	// var_dump($circuits);die();
?>


<?php  $current_options = wp_parse_args(  get_option( 'busiprof_theme_options', array() ), theme_setup_data() ); 
if( $current_options['home_recentblog_section_enabled']=='on' ) { ?>
<!-- Testimonial & Blog Section -->
<section id="section" class="home-post-latest">
	<div class="container">	
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<?php
					if( $current_options['recent_blog_title'] != '' ) { ?> 
					<h1 class="section-heading">Contactez nous</h1>
					<?php } if( $current_options['recent_blog_description'] !='')  { ?>
					<?php } ?>
				</div>
			</div>
		</div>


	
		<!-- /Section Title -->	
			
		<!-- Blog Post -->				
		<div class="row">
					<div class="col-md-6">
						<div class="post"> 
							<div class="media"> 
								<figure class="post-thumbnail"></figure> 
								<div class="media-body">
									<div class="entry-header">
										<h4 class="entry-title"><a href="https://wp-themes.com/?p=36">Adresse :</a></h4>
									</div>
									<div class="entry-content">
										<p></p><p><!-- Sample Content to Plugin to Template --></p>
										<?php echo $circuits->description;?>
										<?php echo $circuits->description;?>
										<?php echo $circuits->description;?>
									</div>
								</div> 
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="post"> 
							<div class="media"> 
								<figure class="post-thumbnail"></figure> 
								<div class="media-body">
									<?php echo do_shortcode($post->post_content) ; ?>
								</div> 
							</div>
						</div>
					</div>
		<!-- /Blog Post -->
		</div>
		<!-- /Blog Post -->
		</div>
</section>
<!-- End of Testimonial & Blog Section -->
<div class="clearfix"></div>
<?php } ?>
  	

<!-- footer Section of index Testimonial -->
<?php
 get_footer(); ?>