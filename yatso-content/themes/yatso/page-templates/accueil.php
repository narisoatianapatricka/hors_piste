<?php
/**
 * Template Name: Accueil
 *
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

get_header(); 
	$current_options = wp_parse_args(  get_option( 'busiprof_theme_options', array() ), $busiprof_theme_options );
	// $circuits = CCircuits::list_circuits();
	global $post ;
	$circuits = CCircuits::getBy( 4, $sorting = null, $data_filters = array(), $tax_filters = array(), $meta_filters  = array());
?>
  	
<?php 
$current_options = wp_parse_args(  get_option( 'busiprof_theme_options', array() ), theme_setup_data() );
if( $current_options['home_banner_strip_enabled'] == 'on' && $current_options['slider_head_title'] != '' ) { ?>
<div class="clearfix"></div>
<!-- Slider -->
<?php } if($current_options['slider_image']!='' ) { ?>
<div id="main" role="main">
	<section class="slider">
		<ul class="slides">
				<li>
					<img alt="img" src="<?php echo esc_url($current_options['slider_image']); ?>" />
					<div class="container">
						<div class="slide-caption">
							<?php if($current_options['caption_head']!='') {?>
							<h2><?php echo esc_html($current_options['caption_head']); ?></h2>
							<?php } if($current_options['caption_text']!='') {?>
							<p><?php echo esc_html($current_options['caption_text']); ?></p>
							<?php } ?>	
							<?php if($current_options['readmore_text_link']!=''){ ?>
							<div><a href="<?php echo esc_url($current_options['readmore_text_link']); ?>" 
							<?php if($current_options['readmore_target'] !=false) { ?>
							target="_blank" <?php } ?> class="flex-btn"><?php echo esc_html($current_options['readmore_text']); ?></a>
							</div>
							<?php }?>
						</div>		
					</div>
				</li>
			</ul>		
	</section>
</div>
<!-- End of Slider -->
<div class="clearfix"></div>
<?php } else{
	
	?>
<?php
} if( $current_options['home_banner_strip_enabled'] == 'on') {?>
<section class="header-title"><h2><?php echo esc_html($current_options['slider_head_title']); ?> </h2></section>
<div class="clearfix"></div>
<?php } ?>



<!-- Présentation du site -->
<section id="section" class="portfolio bg-color">
	<div class="container">
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<?php if($current_options['protfolio_tag_line']!='') {?>
					<h1 class="section-heading"><?php echo get_field('titre_bloc_accueil',$post->ID) ?>
					</h1><?php } ?>
					<?php if($current_options['protfolio_description_tag']!='') {?>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- /Section Title -->
	<style>
		@media screen and (max-width: 1040px) {
      .presentation-group{
        padding: 0 30px;
    		}
		}
	</style>
				
		<!-- Portfolio Item -->
	<div class="tab-content main-portfolio-section" id="myTabContent">
		<!-- Portfolio Item -->
			<div class="row">
				<p class="presentation-group"><?php echo get_field('contenu_bloc_accueil',$post->ID) ?></p>
			</div>	
			<!-- <div class="clearfix"></div> -->
		</div>
	</div>
</section>
<!-- Fin Présentation du site -->

<!-- Nos circuits -->

<!-- Portfolio Section -->
<section id="section" class="portfolio bg-color">
	<div class="container">
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<?php if($current_options['protfolio_tag_line']!='') {?>
					<h1 class="section-heading"><?php echo $current_options['protfolio_tag_line']; ?>
					</h1><?php } ?>
					<?php if($current_options['protfolio_description_tag']!='') {?>
					<p><?php echo esc_html($current_options['protfolio_description_tag']); ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- /Section Title -->
		
				
		<!-- Portfolio Item -->
	<div class="tab-content main-portfolio-section" id="myTabContent">
		<!-- Portfolio Item -->
			<div class="row">
				<?php if (isset($circuits) && !empty($circuits)) {
					foreach ($circuits as $itemC) {
						?>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<aside class="post">
										<figure class="post-thumbnail">
											<a href="">
											<?php if(isset($itemC->banniere_image) && !empty($itemC->banniere_image)) {?>
											<img alt="" src="<?php echo $itemC->banniere_image[0]; ?>" class="project_feature_img" />
											<?php } ?>
											</a>
										</figure>
									<div class="portfolio-info">
										<div class="entry-header">
											<h4 class="entry-title">
											<a href="">
												<?php if(isset($itemC->titre) && !empty($itemC->titre)) {
													echo $itemC->titre;}
													?>
											</a>
											</h4>
										</div>
										<div class="entry-content">
											<?php if(isset($itemC->description) && !empty($itemC->description)) {?>
											<p><?php echo $itemC->description ; ?></p>
											<?php } ?>
										</div>
									</div>	
								</aside>
							</div>
						<?php
					}
				} ?>
			<div class="clearfix"></div>
			<div class="col-md-12 col-xs-12">
				<div class="btn-wrap">
					<div class="services_more_btn">
						<a href="">Tous les circuits</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- End of Portfolio Section -->

<!-- Fin nos circuits -->




<!-- footer Section of index Testimonial -->
<?php
 get_footer(); ?>