<?php
/**
 * Template Name: DetailCircuit
 *
 *
 * @package WordPress
 * @subpackage yatso
 * @since yatso 1.0
 * @author : Joel et Narisoa
 */

get_header(); 
	global $post;
	$current_options = wp_parse_args(  get_option( 'busiprof_theme_options', array() ), $busiprof_theme_options );
	// $circuits        = CCircuits::getById($post->ID);
	$circuits        = CCircuits::getById(41);
	// var_dump($circuits);die();
?>


<?php  $current_options = wp_parse_args(  get_option( 'busiprof_theme_options', array() ), theme_setup_data() ); 
if( $current_options['home_recentblog_section_enabled']=='on' ) { ?>
<!-- Testimonial & Blog Section -->
<section id="section" class="home-post-latest">
	<div class="container">	
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<?php
					if( $current_options['recent_blog_title'] != '' ) { ?> 
					<h1 class="section-heading"><?php echo $circuits->titre;?></h1>
					<?php } if( $current_options['recent_blog_description'] !='')  { ?>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- /Section Title -->	
			
		<!-- Blog Post -->				
		<div class="row">
					<div class="col-md-6">
						<div class="post"> 
							<div class="media"> 
								<figure class="post-thumbnail"></figure> 
								<div class="media-body">
																<div class="entry-meta">
									<span class="entry-date"><a href="https://wp-themes.com/?p=19"><time datetime="">Oct 17,2008</time></a></span>
									<span class="comments-link"><a href="https://wp-themes.com/?p=19"><span>Comments Off<span class="screen-reader-text"> on Worth A Thousand Words</span></span></a></span>
																<span class="tag-links"><a href="https://wp-themes.com/?p=19"></a><a href="https://wp-themes.com/?tag=boat" rel="tag">boat</a>, <a href="https://wp-themes.com/?tag=lake" rel="tag">lake</a></span>
																</div>
									
									
									<div class="entry-header">
										<h4 class="entry-title"><a href="https://wp-themes.com/?p=19">Banière d'image</a></h4>
									</div>
									<div class="entry-content">
										<p></p><div style="width: 445px" class="wp-caption alignnone"><img class="wp-image-59" alt="Boat" src="<?php echo $circuits->banniere_image[0];?>" width="435" height="288"><p class="wp-caption-text">Boat</p></div>
										<p></p>
									</div>
								</div> 
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="post"> 
							<div class="media"> 
								<figure class="post-thumbnail">													</figure> 
								<div class="media-body">
									<div class="entry-meta">
										<span class="entry-date"><a href="https://wp-themes.com/?p=36"><time datetime="">Sep 5,2008</time></a></span>
										<span class="comments-link"><a href="https://wp-themes.com/?p=36"><span>Comments Off<span class="screen-reader-text"> on Elements</span></span></a></span>
									</div>
									
									
									<div class="entry-header">
										<h4 class="entry-title"><a href="https://wp-themes.com/?p=36">Description :</a></h4>
									</div>
									<div class="entry-content">
										<p></p><p><!-- Sample Content to Plugin to Template --></p>
										<?php echo $circuits->description;?>
										<?php echo $circuits->description;?>
										<?php echo $circuits->description;?>
										<hr>
										<hr>
									</div>
								</div>
							</div>
						</div>
					</div>
		<!-- /Blog Post -->
		</div>

		<div class="row">
					<div class="col-md-6">
						<div class="post"> 
							<div class="media"> 
								<figure class="post-thumbnail"></figure> 
								<div class="media-body">
									<div class="entry-header">
										<h4 class="entry-title"><a href="https://wp-themes.com/?p=19">Time line journée image</a></h4>
									</div>
									<div class="entry-content">
										<p></p><div style="width: 445px" class="wp-caption alignnone"><img class="wp-image-59" alt="Boat" src="<?php echo $circuits->banniere_image[0];?>" width="435" height="288"><p class="wp-caption-text">Time line journée :</p></div>
										<p><?php echo $circuits->timeline_journee_texte;?></p>
									</div>
								</div> 
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="post"> 
							<div class="media"> 
								<figure class="post-thumbnail"></figure> 
								<div class="media-body">
									<div class="entry-header">
										<h4 class="entry-title"><a href="https://wp-themes.com/?p=19">Video: </a></h4>
									</div>
									<div class="entry-content">
										<p></p><div style="width: 445px" class="wp-caption alignnone"><iframe  width="435" height="288" src="https://www.youtube.com/embed/F9Bo89m2f6g" frameborder="0" allowfullscreen></iframe><p class="wp-caption-text">Boat</p></div>
										<p></p>
									</div>
								</div> 
							</div>
						</div>
					</div>
		<!-- /Blog Post -->
		</div>

	</div>
</section>
<!-- End of Testimonial & Blog Section -->
<div class="clearfix"></div>
<?php } ?>
  	

<!-- footer Section of index Testimonial -->
<?php
 get_footer(); ?>