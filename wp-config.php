<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'yatso-tours' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`}}}#b]:%y1HOxQ v-jM+vjEIr*B-;Ce[a5a+)7H.|gR>>JCaKt>8#)TC*>=2Iy&' );
define( 'SECURE_AUTH_KEY',  'h,#pa7M[daEwc+&gwy.R_jqk96G>LOFgSp,EtVPC;PF$&Sdbv`pi{62fFFyD8kI~' );
define( 'LOGGED_IN_KEY',    '6cxct3*|0tk<DoN3o_%mjK!``4F#gsEMRP]$N;XH-59^DD/)EmrC}R6}iPV9v8[4' );
define( 'NONCE_KEY',        '+d0@B5ApF.fS2V.p;4qb^n#no*o^kL4YL;=6n~X{^,V&z@)oIExoNlYqses76Hs-' );
define( 'AUTH_SALT',        'pF5ab:+!8]TGu_c}0CXPEVTJH*KdDl^<{u+ZW,=8{V[4*~U+@E:/DmpQ4x|[U4pp' );
define( 'SECURE_AUTH_SALT', '_ZUW_k-FH-q+!~@XR`b!CQ3P^0hp1J0S7#v^j]DLA??zfc%zc:z`:;J1PmW$NVSA' );
define( 'LOGGED_IN_SALT',   ']W<|Ub4ex<_ZX&TTDg$Qw[o0~0%6z#@2kP@mS9~$em9iXmgV(N^+V4%Dd+)g!zaA' );
define( 'NONCE_SALT',       'r%q<[qO3AN2>[rlE[}dyY<uVQV|/o>?c#:`wxdT-]`o9{YPj/CWi5ZEJ>hY_LTNM' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'yatso_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
define ( 'WP_CONTENT_FOLDERNAME' , 'yatso-content');
define ('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
define('WP_SITEURL', 'http://localhost/yatso-tours/srcs');
define('WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME);
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
